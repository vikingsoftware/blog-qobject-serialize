Simple project to serialize/deserialize a QObject

[![build status](https://gitlab.com/vikingsoft/blog-qobject-serialize/badges/master/build.svg)](https://gitlab.com/vikingsoft/blog-qobject-serialize/commits/master)

[![coverage report](https://gitlab.com/vikingsoft/blog-qobject-serialize/badges/master/coverage.svg)](https://gitlab.com/vikingsoft/blog-qobject-serialize/commits/master)
