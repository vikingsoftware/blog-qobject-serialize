CONFIG += c++11

linux {
    QMAKE_CXXFLAGS += -Werror -Wall -Wextra -Wshadow -Wnon-virtual-dtor -pedantic
}

win32 {
    QMAKE_CXXFLAGS += /W4
}
