/****************************************************************************
** This is free and unencumbered software released into the public domain.
**
** Anyone is free to copy, modify, publish, use, compile, sell, or
** distribute this software, either in source code form or as a compiled
** binary, for any purpose, commercial or non-commercial, and by any
** means.
**
** In jurisdictions that recognize copyright laws, the author or authors
** of this software dedicate any and all copyright interest in the
** software to the public domain. We make this dedication for the benefit
** of the public at large and to the detriment of our heirs and
** successors. We intend this dedication to be an overt act of
** relinquishment in perpetuity of all present and future rights to this
** software under copyright law.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
** OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
** ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
** OTHER DEALINGS IN THE SOFTWARE.
**
** For more information, please refer to <http://unlicense.org>
****************************************************************************/

#include "qtdeserialize.h"
#include "qttypesobject.h"
#include "simpleobject.h"

#include <QHash>
#include <QString>
#include <QtTest>

class ReadJsonTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void readWrongData();
    void readArray();
    void readWrongClass();
    void readOtherJson();
    void readDynamicProperties();
    void readSimplePropertiesOfOneObject();
    void readNestedQObject();
    void readNestedInvalidObject();
    void setFactory();
    void readNestedCustomObject();
    void readNestedUnknownObject();
    void readNestedInvalidNestedObject();
    void readQtTypes();
};

class MyFactory : public vs::ObjectFactory
{
public:
    MyFactory()
    {
        m_classes[QStringLiteral("SimpleObject")] = []() { return new SimpleObject; };
    }

    QObject* create(const QString& className) const override
    {
        auto it = m_classes.find(className);
        if (it != m_classes.end()) {
            return it.value()();
        } else {
            return vs::ObjectFactory::create(className);
        }
    }

private:
    QHash<QString, std::function<QObject*()>> m_classes;
};


void ReadJsonTest::readWrongData()
{
    vs::QtDeSerialize reader;
    bool ok = reader.deserialize(nullptr, "error");
    QCOMPARE(ok, false);

    QObject obj;
    ok = reader.deserialize(&obj, "error");
    QCOMPARE(ok, false);
}

void ReadJsonTest::readWrongClass()
{
    QByteArray data{R"({"age":99,"className":"Person"})"};
    QObject obj;
    vs::QtDeSerialize reader;
    bool ok = reader.deserialize(&obj, data);
    QCOMPARE(ok, false);
}

void ReadJsonTest::readOtherJson()
{
    QByteArray data{R"({"age":99})"};
    QObject obj;
    vs::QtDeSerialize reader;
    bool ok = reader.deserialize(&obj, data);
    QCOMPARE(ok, false);
}

void ReadJsonTest::readArray()
{
    QByteArray data{R"([{"age":99,"className":"Person"}])"};
    QObject obj;
    vs::QtDeSerialize reader;
    bool ok = reader.deserialize(&obj, data);
    QCOMPARE(ok, false);
}

void ReadJsonTest::readDynamicProperties()
{
    QByteArray data{R"({"age":99,"className":"QObject","name":"qwerty"})"};
    QObject obj;
    vs::QtDeSerialize reader;
    bool ok = reader.deserialize(&obj, data);
    QCOMPARE(ok, true);
    QCOMPARE(obj.property("age").toInt(), 99);
    QCOMPARE(obj.property("name").toString(), QStringLiteral("qwerty"));
}

void ReadJsonTest::readSimplePropertiesOfOneObject()
{
    QByteArray data{R"({"age":99,"className":"SimpleObject","isValid":true,"name":"qwerty"})"};
    SimpleObject obj;

    vs::QtDeSerialize reader;
    bool ok = reader.deserialize(&obj, data);
    QCOMPARE(ok, true);
    QCOMPARE(obj.age(), 99);
    QCOMPARE(obj.name(), QStringLiteral("qwerty"));
    QCOMPARE(obj.isValid(), true);
}

void ReadJsonTest::readNestedQObject()
{
    QByteArray data{R"({"className":"QObject","subObj":{"age":99,"className":"QObject","name":"qwerty"}})"};
    QObject obj;
    vs::QtDeSerialize reader;
    bool ok = reader.deserialize(&obj, data);
    QCOMPARE(ok, true);
    QObject* subObj = obj.property("subObj").value<QObject*>();
    QVERIFY(subObj != nullptr);
    QCOMPARE(subObj->property("age").toInt(), 99);
    QCOMPARE(subObj->property("name").toString(), QStringLiteral("qwerty"));
}

void ReadJsonTest::readNestedInvalidObject()
{
    QByteArray data{R"({"className":"QObject","subObj":{"age":99,"name":"qwerty"}})"};
    QObject obj;
    vs::QtDeSerialize reader;
    bool ok = reader.deserialize(&obj, data);
    QCOMPARE(ok, false);
}

void ReadJsonTest::setFactory()
{
    vs::QtDeSerialize reader;
    std::unique_ptr<MyFactory> factory(new MyFactory);
    reader.setObjectFactory(std::move(factory));
    QVERIFY(!factory);
}

void ReadJsonTest::readNestedCustomObject()
{
    QByteArray data{
        R"({"className":"QObject","subObj":{"age":99,"className":"SimpleObject","isValid":true,"name":"qwerty"}})"};
    QObject obj;
    vs::QtDeSerialize reader;
    std::unique_ptr<MyFactory> factory(new MyFactory);
    reader.setObjectFactory(std::move(factory));

    bool ok = reader.deserialize(&obj, data);
    QCOMPARE(ok, true);
    SimpleObject* subObj = obj.property("subObj").value<SimpleObject*>();
    QVERIFY(subObj != nullptr);
    QCOMPARE(subObj->property("age").toInt(), 99);
    QCOMPARE(subObj->property("isValid").toBool(), true);
    QCOMPARE(subObj->property("name").toString(), QStringLiteral("qwerty"));
}

void ReadJsonTest::readNestedUnknownObject()
{
    QByteArray data{R"({"className":"QObject","subObj":{"age":99,"className":"OtherObject"}})"};
    QObject obj;
    vs::QtDeSerialize reader;

    bool ok = reader.deserialize(&obj, data);
    QCOMPARE(ok, false);
}

void ReadJsonTest::readNestedInvalidNestedObject()
{
    QByteArray data{R"({"className":"QObject","subObj":{"className":"QObject","subObj":{}}})"};
    QObject obj;
    vs::QtDeSerialize reader;
    bool ok = reader.deserialize(&obj, data);
    QCOMPARE(ok, false);
}

void ReadJsonTest::readQtTypes()
{
    QtTypesObject obj;
    vs::QtDeSerialize reader;

    QByteArray data{R"({"className":"QtTypesObject","color":"#0000ff","date":"2016-10-20"})"};
    bool ok = reader.deserialize(&obj, data);
    QCOMPARE(ok, true);
    QCOMPARE(obj.color(), QColor(0, 0, 255));
    QCOMPARE(obj.date(), QDate(2016, 10, 20));
}

QTEST_APPLESS_MAIN(ReadJsonTest)

#include "tst_readjsontest.moc"
