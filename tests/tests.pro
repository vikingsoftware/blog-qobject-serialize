include( ../common.pri )

TEMPLATE = subdirs

SUBDIRS += \
    testObjects \
    writeJson \
    readJson

readJson.depends = testObjects
writeJson.depends = testObjects
