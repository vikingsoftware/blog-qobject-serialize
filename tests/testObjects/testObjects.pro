include( ../../common.pri )
include( ../../coverage.pri )

TARGET = testObjects
TEMPLATE = lib

DEFINES += TESTOBJECTS_LIBRARY

SOURCES += \
    simpleobject.cpp \
    qttypesobject.cpp

HEADERS +=\
    testobjects_global.h \
    simpleobject.h \
    qlistobject.h \
    qttypesobject.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
