/****************************************************************************
** This is free and unencumbered software released into the public domain.
**
** Anyone is free to copy, modify, publish, use, compile, sell, or
** distribute this software, either in source code form or as a compiled
** binary, for any purpose, commercial or non-commercial, and by any
** means.
**
** In jurisdictions that recognize copyright laws, the author or authors
** of this software dedicate any and all copyright interest in the
** software to the public domain. We make this dedication for the benefit
** of the public at large and to the detriment of our heirs and
** successors. We intend this dedication to be an overt act of
** relinquishment in perpetuity of all present and future rights to this
** software under copyright law.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
** OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
** ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
** OTHER DEALINGS IN THE SOFTWARE.
**
** For more information, please refer to <http://unlicense.org>
****************************************************************************/

#include "simpleobject.h"

SimpleObject::SimpleObject(QObject* parent)
    : QObject(parent)
    , m_age(-1)
    , m_isValid(false)
{
}

int SimpleObject::age() const
{
    return m_age;
}

bool SimpleObject::isValid() const
{
    return m_isValid;
}

QString SimpleObject::name() const
{
    return m_name;
}

void SimpleObject::setAge(int age)
{
    if (m_age == age) {
        return;
    }

    m_age = age;
    emit ageChanged(age);
}

void SimpleObject::setIsValid(bool isValid)
{
    if (m_isValid == isValid) {
        return;
    }

    m_isValid = isValid;
    emit isValidChanged(isValid);
}

void SimpleObject::setName(QString name)
{
    if (m_name == name) {
        return;
    }

    m_name = name;
    emit nameChanged(name);
}
