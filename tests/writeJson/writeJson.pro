include( ../test_common.pri )
include( ../../coverage.pri )

TARGET = tst_writeJsonTest

SOURCES += \
    tst_writejsontest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../lib/release/ -lqtserialize
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../lib/debug/ -lqtserialize
else:unix: LIBS += -L$$OUT_PWD/../../lib/ -lqtserialize

INCLUDEPATH += $$PWD/../../lib
DEPENDPATH += $$PWD/../../lib

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../testObjects/release/ -ltestObjects
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../testObjects/debug/ -ltestObjects
else:unix: LIBS += -L$$OUT_PWD/../testObjects/ -ltestObjects

INCLUDEPATH += $$PWD/../testObjects
DEPENDPATH += $$PWD/../testObjects
