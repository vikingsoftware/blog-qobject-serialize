/****************************************************************************
** This is free and unencumbered software released into the public domain.
**
** Anyone is free to copy, modify, publish, use, compile, sell, or
** distribute this software, either in source code form or as a compiled
** binary, for any purpose, commercial or non-commercial, and by any
** means.
**
** In jurisdictions that recognize copyright laws, the author or authors
** of this software dedicate any and all copyright interest in the
** software to the public domain. We make this dedication for the benefit
** of the public at large and to the detriment of our heirs and
** successors. We intend this dedication to be an overt act of
** relinquishment in perpetuity of all present and future rights to this
** software under copyright law.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
** OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
** ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
** OTHER DEALINGS IN THE SOFTWARE.
**
** For more information, please refer to <http://unlicense.org>
****************************************************************************/

#include "qlistobject.h"
#include "qtserialize.h"
#include "qttypesobject.h"
#include "simpleobject.h"

#include <QDate>
#include <QString>
#include <QtGlobal>
#include <QtTest>

class WriteJsonTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void checkInvalidObject();
    void writeSimplePropertiesOfOneObject();
    void writeDynamicProperties();
    void writeNonBlackListet();
    void writeSubObjects();
    void ignoreUnsupportedTypes();
    void writeQListProperty();
    void writeQtTypes();
};

void WriteJsonTest::checkInvalidObject()
{
    vs::QtSerialize serialize;
    QByteArray result = serialize.serialize(nullptr);
    QByteArray intended{"{}"};
    QCOMPARE(QString(result), QString(intended));
}

void WriteJsonTest::writeSimplePropertiesOfOneObject()
{
    SimpleObject obj;
    obj.setAge(99);
    obj.setIsValid(true);
    obj.setName("qwerty");

    vs::QtSerialize serialize;
    QByteArray result = serialize.serialize(&obj);
    QByteArray intended{R"({"age":99,"className":"SimpleObject","isValid":true,"name":"qwerty"})"};
    QCOMPARE(QString(result), QString(intended));
}

void WriteJsonTest::writeDynamicProperties()
{
    QObject obj;
    obj.setProperty("age", 99);
    obj.setProperty("length", 12.3);
    obj.setProperty("name", "qwerty");

    vs::QtSerialize serialize;
    QByteArray result = serialize.serialize(&obj);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 7, 0))
    QByteArray intended{R"({"age":99,"className":"QObject","length":12.3,"name":"qwerty"})"};
#else
    QByteArray intended{R"({"age":99,"className":"QObject","length":12.300000000000001,"name":"qwerty"})"};
#endif
    QCOMPARE(result, intended);
}

void WriteJsonTest::writeNonBlackListet()
{
    QObject obj;
    obj.setObjectName("object1");
    obj.setProperty("age", 99);
    obj.setProperty("name", "qwerty");

    vs::QtSerialize serialize;
    serialize.clearBlackList();
    serialize.addBlackListItem("name");
    QByteArray result = serialize.serialize(&obj);
    QByteArray intended{R"({"age":99,"className":"QObject","objectName":"object1"})"};
    QCOMPARE(result, intended);
}

void WriteJsonTest::writeSubObjects()
{
    QObject obj;
    obj.setProperty("name", "qwerty");
    SimpleObject* subObject = new SimpleObject();
    subObject->setParent(&obj);
    obj.setProperty("BabyObject", QVariant::fromValue<QObject*>(subObject));
    subObject->setAge(14);
    subObject->setName("John Doe");

    vs::QtSerialize serialize;
    QByteArray result = serialize.serialize(&obj);
    QByteArray intended{
        R"({"BabyObject":{"age":14,"className":"SimpleObject","isValid":false,"name":"John Doe"},"className":"QObject","name":"qwerty"})"};
    QCOMPARE(result, intended);
}

void WriteJsonTest::ignoreUnsupportedTypes()
{
    QObject obj;
    obj.setProperty("foo", QRect(0, 0, 20, 10));

    vs::QtSerialize serialize;
    QByteArray result = serialize.serialize(&obj);
    QByteArray intended{R"({"className":"QObject"})"};
    QCOMPARE(result, intended);
}

void WriteJsonTest::writeQListProperty()
{
    QListObject obj;
    obj.setList({QString("bar"), QString("foo")});

    vs::QtSerialize serialize;
    QByteArray result = serialize.serialize(&obj);
    QByteArray intended{R"({"className":"QListObject","list":["bar","foo"]})"};
    QCOMPARE(result, intended);
}

void WriteJsonTest::writeQtTypes()
{
    QtTypesObject obj;
    vs::QtSerialize serialize;

    QByteArray result = serialize.serialize(&obj);
    QByteArray intended{R"({"className":"QtTypesObject","color":"#00ff00","date":"2016-10-18"})"};
    QCOMPARE(result, intended);
}

QTEST_APPLESS_MAIN(WriteJsonTest)

#include "tst_writejsontest.moc"
