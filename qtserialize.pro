include( common.pri )
include( coverage.pri )

TEMPLATE = subdirs

SUBDIRS = lib \
    tests

tests.depends = lib
