/****************************************************************************
** This is free and unencumbered software released into the public domain.
**
** Anyone is free to copy, modify, publish, use, compile, sell, or
** distribute this software, either in source code form or as a compiled
** binary, for any purpose, commercial or non-commercial, and by any
** means.
**
** In jurisdictions that recognize copyright laws, the author or authors
** of this software dedicate any and all copyright interest in the
** software to the public domain. We make this dedication for the benefit
** of the public at large and to the detriment of our heirs and
** successors. We intend this dedication to be an overt act of
** relinquishment in perpetuity of all present and future rights to this
** software under copyright law.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
** OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
** ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
** OTHER DEALINGS IN THE SOFTWARE.
**
** For more information, please refer to <http://unlicense.org>
****************************************************************************/

#ifndef QTDESERIALIZE_H
#define QTDESERIALIZE_H

#include "qtserialize_global.h"

#include <memory>

class QByteArray;
class QObject;
class QString;

namespace vs
{
/**
 * @brief Factory class to create custom objects
 * @see QtDeSerialize QtDeSerialize::setObjectFactory
 */
class ObjectFactory
{
public:
    virtual ~ObjectFactory() = default;

    virtual QObject* create(const QString& className) const;
};

/**
 * @brief The QtDeSerialize class reads a QObject written by QtSerialize
 */
class QTSERIALIZESHARED_EXPORT QtDeSerialize
{
public:
    QtDeSerialize();
    ~QtDeSerialize();

    /**
     * Read a object from JSON data
     */
    bool deserialize(QObject* obj, const QByteArray& data);

    /**
     * Factory class to create QObject derived classes.
     * @param factory
     */
    void setObjectFactory(std::unique_ptr<ObjectFactory> factory);

private:
    class QtDeSerializePrivate;
    Q_DISABLE_COPY(QtDeSerialize)
    Q_DECLARE_PRIVATE(QtDeSerialize)
    QtDeSerializePrivate* const d_ptr;
};
}
#endif
