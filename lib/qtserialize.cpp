/****************************************************************************
** This is free and unencumbered software released into the public domain.
**
** Anyone is free to copy, modify, publish, use, compile, sell, or
** distribute this software, either in source code form or as a compiled
** binary, for any purpose, commercial or non-commercial, and by any
** means.
**
** In jurisdictions that recognize copyright laws, the author or authors
** of this software dedicate any and all copyright interest in the
** software to the public domain. We make this dedication for the benefit
** of the public at large and to the detriment of our heirs and
** successors. We intend this dedication to be an overt act of
** relinquishment in perpetuity of all present and future rights to this
** software under copyright law.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
** OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
** ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
** OTHER DEALINGS IN THE SOFTWARE.
**
** For more information, please refer to <http://unlicense.org>
****************************************************************************/

#include "qtserialize.h"

#include <QByteArray>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QMetaProperty>
#include <QObject>
#include <QSet>

namespace vs
{
class QtSerialize::QtSerializePrivate
{
public:
    void insertVariant(QJsonObject* jsonObject, const QString& propertyName, const QVariant& data) const;
    QJsonObject convertQObject(QObject* obj) const;

    QSet<QString> m_blackList = {{QStringLiteral("objectName")}};
};

void QtSerialize::QtSerializePrivate::insertVariant(
    QJsonObject* jsonObject, const QString& propertyName, const QVariant& data) const
{
    const QMetaType::Type type = static_cast<QMetaType::Type>(data.type());
    QJsonValue jv;
    if (type == QMetaType::QObjectStar) {
        QJsonObject subObject = convertQObject(data.value<QObject*>());
        jv = subObject;
    } else if (type == QMetaType::Bool || type == QMetaType::Double || type == QMetaType::Float
        || type == QMetaType::Int || type == QMetaType::UInt || type == QMetaType::LongLong
        || type == QMetaType::ULongLong || type == QMetaType::QString || type == QMetaType::QByteArray) {
        jv = QJsonValue::fromVariant(data);
    } else if (type == QMetaType::QVariantList) {
        QJsonArray array = QJsonArray::fromVariantList(data.value<QVariantList>());
        jv = array;
    } else if (data.canConvert<QString>()) {
        jv = QJsonValue::fromVariant(data);
    } else {
        qWarning() << "Unable to convert property <" + propertyName + "> to JSON - unkown/unsupported type ";
    }

    if (!jv.isNull()) {
        jsonObject->insert(propertyName, jv);
    }
}

QJsonObject QtSerialize::QtSerializePrivate::convertQObject(QObject* obj) const
{
    if (obj == nullptr) {
        return {};
    }

    QJsonObject jsonObject;
    const QMetaObject* metaObj = obj->metaObject();
    jsonObject.insert(QStringLiteral("className"), QJsonValue(metaObj->className()));

    for (int i = 0; i < metaObj->propertyCount(); ++i) {
        const QString propertyName = metaObj->property(i).name();
        if (!m_blackList.contains(propertyName)) {
            QVariant data = obj->property(propertyName.toLatin1().data());
            insertVariant(&jsonObject, propertyName, data);
        }
    }

    for (const QByteArray& propertyName : obj->dynamicPropertyNames()) {
        if (!m_blackList.contains(propertyName)) {
            insertVariant(&jsonObject, propertyName, obj->property(propertyName.data()));
        }
    }
    return jsonObject;
}


QtSerialize::QtSerialize()
    : d_ptr{new QtSerializePrivate}
{
}

QtSerialize::~QtSerialize()
{
    delete d_ptr;
}

QByteArray QtSerialize::serialize(QObject* obj)
{
    Q_D(QtSerialize);
    QJsonObject jsonObject = d->convertQObject(obj);

    QJsonDocument doc;
    doc.setObject(jsonObject);
    return doc.toJson(QJsonDocument::Compact);
}

void QtSerialize::clearBlackList()
{
    Q_D(QtSerialize);
    d->m_blackList.clear();
}

void QtSerialize::addBlackListItem(const QString& name)
{
    Q_D(QtSerialize);
    d->m_blackList.insert(name);
}
}
