/****************************************************************************
** This is free and unencumbered software released into the public domain.
**
** Anyone is free to copy, modify, publish, use, compile, sell, or
** distribute this software, either in source code form or as a compiled
** binary, for any purpose, commercial or non-commercial, and by any
** means.
**
** In jurisdictions that recognize copyright laws, the author or authors
** of this software dedicate any and all copyright interest in the
** software to the public domain. We make this dedication for the benefit
** of the public at large and to the detriment of our heirs and
** successors. We intend this dedication to be an overt act of
** relinquishment in perpetuity of all present and future rights to this
** software under copyright law.
**
** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
** EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
** IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
** OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
** ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
** OTHER DEALINGS IN THE SOFTWARE.
**
** For more information, please refer to <http://unlicense.org>
****************************************************************************/

#include "qtdeserialize.h"

#include <QByteArray>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QObject>

namespace vs
{
QObject* ObjectFactory::create(const QString& className) const
{
    if (className == "QObject") {
        return new QObject;
    }

    qDebug() << "Unknown class to create: " << className;
    return nullptr;
}


class QtDeSerialize::QtDeSerializePrivate
{
public:
    QtDeSerializePrivate()
        : m_factory(new ObjectFactory)
    {
    }

    bool parseObject(const QJsonObject& jo, QObject* obj);

    std::unique_ptr<ObjectFactory> m_factory;
};

bool QtDeSerialize::QtDeSerializePrivate::parseObject(const QJsonObject& jo, QObject* obj)
{
    const QString classNameKey("className");
    if (!jo.contains(classNameKey)) {
        qDebug() << "JSON object is not a serialized one";
        return false;
    }
    const QString className = jo.value(classNameKey).toString();
    if (className != obj->metaObject()->className()) {
        qDebug() << "Wrong class to deserialize " + className + " | " + obj->metaObject()->className();
        return false;
    }

    for (const QString& key : jo.keys()) {
        if (key != classNameKey) {
            QJsonValue jsonValue = jo.value(key);
            if (jsonValue.isBool() || jsonValue.isDouble() || jsonValue.isString()) {
                obj->setProperty(key.toLatin1().data(), jsonValue.toVariant());
            } else if (jsonValue.isObject()) {
                QJsonObject jsonObject = jsonValue.toObject();
                QObject* subObj = m_factory->create(jsonObject.value(classNameKey).toString());
                if (subObj == nullptr) {
                    return false;
                }

                subObj->setParent(obj);

                bool ok = parseObject(jsonObject, subObj);
                if (!ok) {
                    return false;
                }
                obj->setProperty(key.toLatin1().data(), QVariant::fromValue(subObj));
            } else {
                qDebug() << "Unsupported JSON type (" << jsonValue.type() << ") for key:" << key;
                return false;
            }
        }
    }

    return true;
}


QtDeSerialize::QtDeSerialize()
    : d_ptr(new QtDeSerializePrivate)

{
}
QtDeSerialize::~QtDeSerialize()
{
    delete d_ptr;
}

bool QtDeSerialize::deserialize(QObject* obj, const QByteArray& data)
{
    Q_D(QtDeSerialize);

    if (obj == nullptr) {
        qDebug() << "No target object to deserialize to";
        return false;
    }

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(data, &error);

    if (error.error != QJsonParseError::NoError) {
        qDebug() << error.errorString();
        return false;
    }

    if (doc.isObject()) {
        QJsonObject jo = doc.object();
        return d->parseObject(jo, obj);
    } else {
        qDebug() << "JSON data is not an object";
        return false;
    }
}

void QtDeSerialize::setObjectFactory(std::unique_ptr<ObjectFactory> factory)
{
    Q_D(QtDeSerialize);
    d->m_factory = std::move(factory);
}
}
